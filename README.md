JwtSample Project
---------------------
Gereksinimler:
* Java 17
* Postgres Db (db name: jwt (application.yml dosyasından güncellenebilir) )
* Maven


Yapılan İşlemler:
* User tablosu
* User lara ait login veya servis hareket logu
* Uygulama başlarken yeni create ediliyor ise default olarak "username: admin, password: admin" şeklinde kullanıcı oluşturulmaktadır.
* Uygulama sunucunda logların görüntülenebilmesi için Logger Interceptor eklenmiştir.
* Cors Filter eklenerek requestlerdeki bazı standartlar belirlenmiştir.
* Global Exception Advice eklenerek herhangi bir nedenden dolayı uygulamada alınan hataların standart bir formatta kullanıcının 
  görebilmesi sağlanmıştır.
* Swagger-api eklenmiştir.
* User entity objesi spring UserDetails den implements edilmiştir.
* User hareket tipleri bir enum ile desteklenmiştir.
* Security Configte basit bir yetkilendirme yapılarak admin servislerine sadece ROLE_ADMIN grubunun erişmesi sağlanmıştır.
* Repository ve Service katmanları herhangi bir abstract class lar ile implement edilmeden yazılmıştır. Bu nedenle transaction işlemi 
  olan classlara Transactional annotationu eklenmiştir.
* Token içerisine role ve user bilgileri eklenmiştir. Bu sayede alınan token bilgisi ile hangi role bağlı olduğunu filterdan geçirebilmekteyiz. 
  Ayrıca token ile direk aktif userName bilgisi alınarak işlemler yapılabilmektedir.
* Servis dönüşleri eklemiş olduğum "DataResult" objesi ile standart olacak şekilde düzenlenmiştir.
* Bazı işlemler throw ile bazıları (info vs.) logger ile desteklenmiştir.


Projeyi çalıştırmak için
------------------------
* Postgres db yi kurunuz (portable da olabilir) jwt adında bir database oluşturun
* Projeyi açın herhangi bir configrasyona ihtiyaç duymadan start edebilirsiniz.
* Bu aşamada kullanmış olduğunuz ide otomatik Spring configrasyonu eklemedi ise add configration seçeneği ile SpringBoot
  configrasyonu ekleyiniz. Application pathi olarak "com.jwtSample.JwtSampleApplication" vermeniz yeterli olacaktır.
