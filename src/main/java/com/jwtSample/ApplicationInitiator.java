package com.jwtSample;

import com.jwtSample.domain.JwtUser;
import com.jwtSample.modules.jwtuser.JwtUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ApplicationInitiator implements CommandLineRunner {


    @Autowired
    JwtUserService jwtUserService;

    public ApplicationInitiator() {

    }


    @Override
    public void run(String... args) throws Exception {
        checkFirstAdminUser();
    }


    private void checkFirstAdminUser() {
        JwtUser u = (JwtUser) jwtUserService.findByUsername("admin");
        if (u == null) {
            JwtUser kullanici = new JwtUser();
            kullanici.setId(1L);
            kullanici.setUsername("admin");
            kullanici.setPassword(new BCryptPasswordEncoder().encode("admin"));
            kullanici.setFirstName("Admin");
            kullanici.setLastName("Admin");
            kullanici.setRole("ADMIN");
            jwtUserService.save(kullanici);
        }
    }


}
