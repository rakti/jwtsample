package com.jwtSample.advice;

import com.jwtSample.base.DataResult;
import com.jwtSample.base.NameValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExAdvice extends ResponseEntityExceptionHandler {

    Logger log = LogManager.getLogger(this.getClass());

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        log.error("handleConstraintViolationException -> " + ex.getMessage(), ex, request);
        return getConstraintViolationExceptionResult(ex);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        List<NameValue> errorList = new ArrayList<>();

        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errorList.add(new NameValue(error.getField(), error.getDefaultMessage()));
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errorList.add(new NameValue(error.getObjectName(), error.getDefaultMessage()));
        }
        DataResult res = new DataResult("", "Eksik Alan");
        res.setMessageList(errorList);
        return handleExceptionInternal(
                ex, res, headers, HttpStatus.BAD_REQUEST, request);
    }


    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error("handleIllegalArgumentException -> " + ex.getMessage(), ex, request);
        log.error("handleIllegalArgumentException  -> " + ex.getStackTrace()[0].getFileName() + ": " + ex.getStackTrace()[0].getLineNumber(), "request");
        return new ResponseEntity<Object>(
                new DataResult("", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity<Object> handleTransactionSystemException(TransactionSystemException ex, WebRequest request) {
        log.error("handleTransactionSystemException -> " + ex.getMessage(), ex, request);
        log.error("handleTransactionSystemException  -> " + ex.getStackTrace()[0].getFileName() + ": " + ex.getStackTrace()[0].getLineNumber(), "request");
        if (ex instanceof TransactionSystemException && ((TransactionSystemException) ex).getOriginalException().getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException exception = new ConstraintViolationException(((TransactionSystemException) ex).getOriginalException().getCause().getMessage(), ((ConstraintViolationException) ((TransactionSystemException) ex).getOriginalException().getCause()).getConstraintViolations());
            return getConstraintViolationExceptionResult(exception);
        } else {
            return new ResponseEntity<Object>(
                    new DataResult("", ex.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAllOtherException(Exception ex, WebRequest request) {
        log.error("handleAllOtherException -> " + ex.getMessage(), ex, request);
        log.error("handleAllOtherException  -> " + ex.getStackTrace()[0].getFileName() + ": " + ex.getStackTrace()[0].getLineNumber(), "request");
        return new ResponseEntity<Object>(
                new DataResult("", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }




    private ResponseEntity<Object> getConstraintViolationExceptionResult(ConstraintViolationException ex) {
        DataResult res = new DataResult();
        res.setSuccess(false);
        res.setErrorCode("");
        List<NameValue> errorList = new ArrayList<>();
        for (ConstraintViolation<?> item : ex.getConstraintViolations()) {
            errorList.add(new NameValue(
                    item.getRootBeanClass().getSimpleName() + "." + item.getPropertyPath(),
                    item.getMessage()
            ));
        }
        res.setMessageList(errorList);
        return new ResponseEntity<Object>(res, HttpStatus.BAD_REQUEST);
    }



}
