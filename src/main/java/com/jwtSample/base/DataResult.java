package com.jwtSample.base;


import java.util.List;

public class DataResult<T> {

    private boolean success;
    private T data;
    private String errorCode;
    private String errorMessage;
    private List<NameValue> messageList;


    public DataResult() {
    }

    public DataResult(boolean success) {
        this.success = success;
    }


    public DataResult(T data) {
        this.data = data;
        this.success = true;
    }

    public DataResult(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.success = false;
    }

    public DataResult(boolean success, String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.success = success;
    }



    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<NameValue> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<NameValue> messageList) {
        this.messageList = messageList;
    }
}
