package com.jwtSample.base;


import com.fasterxml.jackson.annotation.JsonIgnore;

public class NameValue {

    private String name;
    private Object value;


    public NameValue() {

    }

    public NameValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @JsonIgnore
    public String getStringValue() {
        return value.toString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
