package com.jwtSample.config;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;


@Component
public class JwtTokenUtil {

    private  static String stringKeyStatic;
    @Value( "${jwt.stringkey}" )
    public void setStringKeyStatic(String name){
        JwtTokenUtil.stringKeyStatic = name;
    }

    private  static String jwtIssuerStatic;
    @Value("${jwt.jwtIssuer}")
    public void setJwtIssuerStatic(String name){
        JwtTokenUtil.jwtIssuerStatic = name;
    }

    private  static int jwtExpiryMinutes;
    @Value("${jwt.jwtExpiryMinutes}")
    public void setJwtExpiryMinutes(int name){
        if (name == 0) {
            name = 60;
        }
        JwtTokenUtil.jwtExpiryMinutes = name;
    }

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }
    public String getClaimFromToken(String token, String key) {
        Claims allClaimsFromToken = getAllClaimsFromToken(token);
        return allClaimsFromToken.get(key).toString();
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(stringKeyStatic)
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(String username, String role) {
        return doGenerateToken(username, role);
    }

    private String doGenerateToken(String username, String role) {

        Claims claims = Jwts.claims().setSubject(username);
//        claims.put("scopes", user.getUserRoleList().stream().map(x -> x.getRole().getName()).collect(Collectors.toList()));
        claims.put("username", username);
        claims.put("role", role);
//        claims.put("name", user.getFullName());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuer(jwtIssuerStatic)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(DateUtils.addMinutes(new Date(System.currentTimeMillis()),jwtExpiryMinutes))
                .signWith(SignatureAlgorithm.HS256, stringKeyStatic)
                .compact();
    }

    public Boolean validateToken(String token) {
        final String username = getUsernameFromToken(token);
        return ( !isTokenExpired(token));
    }

}
