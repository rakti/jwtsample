package com.jwtSample.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jwtSample.base.BaseEntity;
import com.jwtSample.enums.ActionType;
import jdk.jfr.Label;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "JWT_USER_ACTION",
        indexes = {
                @Index(columnList = "TYPE")
        }
)
public class JwtUserAction extends BaseEntity {

    @NotNull
    @Label("Type")
    @Column(name ="TYPE", nullable = false)
    private ActionType actionType;

    @Label("Action Date")
    @NotNull
    @Column(name = "ACTION_DATE", nullable = false)
    private LocalDateTime actionDate;

    @Label("Description")
    @Column(name = "DESCRIPTION")
    private String description;


    @JsonIgnore
    @Label("Jwt User")
    @JoinColumn(name = "JWT_USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private JwtUser jwtUser;



}
