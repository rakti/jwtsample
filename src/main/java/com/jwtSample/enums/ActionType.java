package com.jwtSample.enums;

public enum ActionType {
    LOGIN,
    LOGOUT,
    CALL_SERVICE
}
