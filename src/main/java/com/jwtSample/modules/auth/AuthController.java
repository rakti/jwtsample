package com.jwtSample.modules.auth;

import com.jwtSample.base.DataResult;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public DataResult login(@RequestBody String jSon) throws Exception {
        return authService.login(new JSONObject(jSon));
    }

    @GetMapping("/logout")
    public DataResult setSession() throws Exception {
        return new DataResult( authService.logout());
    }


}
