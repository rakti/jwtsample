package com.jwtSample.modules.auth;

import com.jwtSample.base.DataResult;
import com.jwtSample.config.JwtTokenUtil;
import com.jwtSample.enums.ActionType;
import com.jwtSample.modules.jwtuseraction.JwtUserActionService;
import com.jwtSample.modules.jwtuseraction.data.CreateActionRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class AuthService {

    @PersistenceContext
    EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserActionService userActionService;



    public DataResult login(JSONObject json) throws Exception {
        String username = json.getString("username");
        String password = json.getString("password");
        Authentication authentication = authenticateUser(username, password);
        User principal = (User) authentication.getPrincipal();
        String token = jwtTokenUtil.generateToken(principal.getUsername(), principal.getAuthorities().iterator().next().getAuthority());

        userActionService.createNewAction(CreateActionRequest.builder()
                .type(ActionType.LOGIN)
                .userName(username)
                .build());

        return new DataResult( token);

    }

    public Authentication authenticateUser(String username, String password) {
        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        return authenticate;
    }

    public boolean logout() {
        return true;
    }




}
