package com.jwtSample.modules.jwtuser;

import com.jwtSample.base.DataResult;
import com.jwtSample.domain.JwtUser;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/admin/jwtuser")
public class JwtUserController {

    @Autowired
    JwtUserService service;

    @GetMapping("/findAll")
    public DataResult<Page<JwtUserListDto>> findAll(@QuerydslPredicate(root = JwtUser.class) Predicate predicate,
                                                    @RequestParam(name = "page", defaultValue = "0") int page,
                                                    @RequestParam(name = "size", defaultValue = "100") int size) {
        try {
            Page<JwtUser> pageEntity = service.findAll(predicate,
                    PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "id")));

            List<JwtUserListDto> dtoList = pageEntity.getContent().stream().map(x -> JwtUserListDto.toDto(x)).collect(Collectors.toList());

            PageImpl<JwtUserListDto> pageDto = new PageImpl<>(dtoList, pageEntity.getPageable(), pageEntity.getTotalElements());

            return new DataResult(pageDto);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @GetMapping("/findById/{id}")
    public DataResult<JwtUserDto> findById(@PathVariable("id") Long id) {

        Object o = JwtUserDto.toDto(service.findById(id));
        return new DataResult((JwtUserDto) o);
    }

    @PostMapping("/save")
    public DataResult<JwtUserDto> save(@Valid @RequestBody JwtUserDto dto) {
        JwtUser baseEntity = (JwtUser) dto.fromDto(dto);
        return new DataResult(dto.toDto(service.save(baseEntity)));

    }

    @DeleteMapping("/delete/{id}")
    public DataResult<JwtUserDto> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return new DataResult(true);
    }

}
