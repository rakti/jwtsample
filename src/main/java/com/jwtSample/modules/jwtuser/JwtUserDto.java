package com.jwtSample.modules.jwtuser;

import com.jwtSample.domain.JwtUser;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class JwtUserDto implements Serializable {

    private Long id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String firstName;
    private String lastName;
    private String email;
    @NotNull
    private String role;

    public static JwtUserDto toDto(JwtUser jwtUser) {
        JwtUserDto map = new ModelMapper().map(jwtUser, JwtUserDto.class);
        map.setPassword(null);
        return map;
    }

    public JwtUser fromDto(JwtUserDto dto) {
        JwtUser map = new ModelMapper().map(dto, JwtUser.class);
        map.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));

        return map;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
