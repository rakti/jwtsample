package com.jwtSample.modules.jwtuser;

import com.jwtSample.domain.JwtUser;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JwtUserRepository extends CrudRepository<JwtUser, Long>,
        QuerydslPredicateExecutor<JwtUser> {

    Optional<JwtUser> findByUsername(String username);
}
