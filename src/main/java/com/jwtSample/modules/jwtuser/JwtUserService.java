package com.jwtSample.modules.jwtuser;

import com.jwtSample.domain.JwtUser;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class JwtUserService implements UserDetailsService {

    @PersistenceContext
    EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }


    @Autowired
    JwtUserRepository repository;

    public Page<JwtUser> findAll(Predicate predicate, Pageable pageable) {
        return repository.findAll(predicate, pageable);
    }

    public JwtUser findById(Long id) {
        Optional<JwtUser> entity = repository.findById(id);
        try {
            return entity.get();
        } catch (Exception ex) {
            throw new EntityNotFoundException(id.toString());
        }
    }

    public JwtUser findByUsername(String username) {
        JwtUser entity = repository.findByUsername(username).orElse(null);
        return entity;
    }

    public JwtUser save(JwtUser entity) {
        return (JwtUser) repository.save(entity);
    }

    public void delete(Long id) {
        JwtUser entity = findById(id);
        repository.delete(entity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        JwtUser user = repository.findByUsername(username).orElse(null);
        if (user != null) {
            List<SimpleGrantedAuthority> authorityList = new ArrayList<SimpleGrantedAuthority>();
            authorityList.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorityList);
        } else {
            throw new UsernameNotFoundException("User Not Found");
        }

    }


}
