package com.jwtSample.modules.jwtuseraction;

import com.jwtSample.base.DataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value ="/userAction")
public class JwtUserActionController {

    @Autowired
    private JwtUserActionService service;

    @GetMapping(value = "/listAll")
    public DataResult listAll(HttpServletRequest request) {
        return new DataResult(service.getHistory(request));

    }
}
