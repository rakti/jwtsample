package com.jwtSample.modules.jwtuseraction;

import com.jwtSample.domain.JwtUserAction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JwtUserActionRepository extends CrudRepository<JwtUserAction, Long> {

    List<JwtUserAction> findByJwtUserId(Long userId);

}
