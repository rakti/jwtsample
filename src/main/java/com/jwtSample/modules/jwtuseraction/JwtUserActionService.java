package com.jwtSample.modules.jwtuseraction;

import com.jwtSample.base.DataResult;
import com.jwtSample.config.JwtTokenUtil;
import com.jwtSample.domain.JwtUser;
import com.jwtSample.domain.JwtUserAction;
import com.jwtSample.enums.ActionType;
import com.jwtSample.modules.jwtuser.JwtUserRepository;
import com.jwtSample.modules.jwtuseraction.data.CreateActionRequest;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;


@Service
@Transactional
public class JwtUserActionService {

    Logger log = LogManager.getLogger(this.getClass());

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private JwtUserActionRepository userActionRepository;

    @Autowired
    private JwtUserRepository userRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public void createNewAction(CreateActionRequest createActionRequest){
        if(checkSaveAction(createActionRequest)){
            JwtUser jwtUser = userRepository.findByUsername(createActionRequest.getUserName()).get();
            userActionRepository.save(JwtUserAction.builder()
                    .actionType(createActionRequest.getType())
                    .actionDate(LocalDateTime.now())
                    .description(createActionRequest.getDescription())
                    .jwtUser(jwtUser)
                    .build());
        } else {
            log.info("Failed create new aciton. type:" + createActionRequest.getType() + ", username:" + createActionRequest.getUserName());
        }

    }

    private boolean checkSaveAction(CreateActionRequest createActionRequest){
        boolean res = true;
        if(createActionRequest==null){
            res = false;
        } else if (ObjectUtils.isEmpty(createActionRequest.getUserName())){
            res = false;
        } else if(createActionRequest.getType()==null){
            res = false;
        }
        return res;
    }

    public DataResult getHistory(HttpServletRequest request){
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        String token = header.split(" ")[1].trim();
        if(ObjectUtils.isEmpty(token)){
            return new DataResult(false,"TOKEN-1","Token bulunamadı. Lütfen giriş yapınız");
        }
        String userName = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser jwtUser = userRepository.findByUsername(userName).get();
        createNewAction(CreateActionRequest.builder()
                .type(ActionType.CALL_SERVICE)
                .description("User get history with in token username")
                .userName(userName)
                .build());

        return new DataResult(userActionRepository.findByJwtUserId(jwtUser.getId()));
    }


}
