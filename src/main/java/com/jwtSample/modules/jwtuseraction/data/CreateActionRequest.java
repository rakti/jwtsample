package com.jwtSample.modules.jwtuseraction.data;

import com.jwtSample.enums.ActionType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreateActionRequest {
    private String userName;
    private ActionType type;
    private String description;
}
