package com.jwtSample.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QJwtUser is a Querydsl query type for JwtUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJwtUser extends EntityPathBase<JwtUser> {

    private static final long serialVersionUID = -347230222L;

    public static final QJwtUser jwtUser = new QJwtUser("jwtUser");

    public final com.jwtSample.base.QBaseEntity _super = new com.jwtSample.base.QBaseEntity(this);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastName = createString("lastName");

    public final StringPath password = createString("password");

    public final StringPath role = createString("role");

    public final StringPath username = createString("username");

    public QJwtUser(String variable) {
        super(JwtUser.class, forVariable(variable));
    }

    public QJwtUser(Path<? extends JwtUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJwtUser(PathMetadata metadata) {
        super(JwtUser.class, metadata);
    }

}

