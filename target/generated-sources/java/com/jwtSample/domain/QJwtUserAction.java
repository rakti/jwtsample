package com.jwtSample.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QJwtUserAction is a Querydsl query type for JwtUserAction
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJwtUserAction extends EntityPathBase<JwtUserAction> {

    private static final long serialVersionUID = 1447484616L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QJwtUserAction jwtUserAction = new QJwtUserAction("jwtUserAction");

    public final com.jwtSample.base.QBaseEntity _super = new com.jwtSample.base.QBaseEntity(this);

    public final DateTimePath<java.time.LocalDateTime> actionDate = createDateTime("actionDate", java.time.LocalDateTime.class);

    public final EnumPath<com.jwtSample.enums.ActionType> actionType = createEnum("actionType", com.jwtSample.enums.ActionType.class);

    public final StringPath description = createString("description");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final QJwtUser jwtUser;

    public QJwtUserAction(String variable) {
        this(JwtUserAction.class, forVariable(variable), INITS);
    }

    public QJwtUserAction(Path<? extends JwtUserAction> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QJwtUserAction(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QJwtUserAction(PathMetadata metadata, PathInits inits) {
        this(JwtUserAction.class, metadata, inits);
    }

    public QJwtUserAction(Class<? extends JwtUserAction> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.jwtUser = inits.isInitialized("jwtUser") ? new QJwtUser(forProperty("jwtUser")) : null;
    }

}

